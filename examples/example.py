from kaldi import fstext as fst
from sigra.decode import Decoder
from sigra.TLG_generator import TLGGenerator


def create_grammar(symbols: fst.SymbolTable) -> fst.StdVectorFst:
    """
    Create a small grammar acceptor
    (0) -a-> (1)
    (1) -a-> (2)
    (1) -b-> (2)
    Accepts aa or ab
    """
    grammar = fst.StdVectorFst()
    # change the symbols tables
    grammar.set_input_symbols(symbols)
    grammar.set_output_symbols(symbols)
    state0 = grammar.add_state()
    state1 = grammar.add_state()
    state2 = grammar.add_state()
    grammar.set_start(state0)
    grammar.set_final(state2)
    weight_free = fst.TropicalWeight(0.0)
    grammar.add_arc(state0, fst.StdArc(1, 1, weight_free, state1))
    grammar.add_arc(state1, fst.StdArc(1, 1, weight_free, state2))
    grammar.add_arc(state1, fst.StdArc(2, 2, weight_free, state2))

    return grammar


if __name__ == "__main__":

    # define a matrix to decode
    matrix = [
        [-1e-1, -1e-5, -1e-5],
        [-1e-5, -1e-1, -1e-5],
        [-1e-5, -1e-5, -1e-1],
        [-1e-1, -1e-4, -1e-4],
    ]

    # define a charset
    charset = ["a", "b"]

    # create a generator
    generator = TLGGenerator(charset)

    # create the grammar with the labels symbol table
    grammar = create_grammar(generator.labels)

    # generate the tlg
    tlg = generator.generate_TLG(grammar)

    # create the decoder
    decoder = Decoder(tlg)

    # decode the matrix
    output = decoder.decode_one_matrix(matrix)

    print(f"Decoded word: {output}")
