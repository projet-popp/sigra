from sigra.decode import Decoder
from sigra.TLG_generator import TLGGenerator


class TestDecode:
    def test_decode(self, grammar):
        charset = ["a", "b"]

        matrix = [
            [-1e-1, -1e-5, -1e-5],
            [-1e-5, -1e-1, -1e-5],
            [-1e-5, -1e-5, -1e-1],
            [-1e-1, -1e-4, -1e-4],
        ]

        generator = TLGGenerator(charset)
        grammar.set_input_symbols(generator.labels)
        grammar.set_output_symbols(generator.labels)

        tlg = generator.generate_TLG(grammar)

        decoder = Decoder(tlg, False)

        test = decoder.decode_one_matrix(matrix)

        assert test == "ab"

    def test_failed_decode(self, grammar):
        charset = ["a", "b"]

        matrix = [
            [0.0, -1.0e01, -1.0e01],
        ]

        generator = TLGGenerator(charset)
        grammar.set_input_symbols(generator.labels)
        grammar.set_output_symbols(generator.labels)

        tlg = generator.generate_TLG(grammar)

        decoder = Decoder(tlg, False)

        test = decoder.decode_one_matrix(matrix)

        assert test is None
