import kaldi.fstext as fst
import pytest
from sigra.TLG_generator import TLGException, TLGGenerator


class TestTLGGenerator:
    """
    Sample fsts used here were generated using the old scripts. But can be created manually.
    """

    def test_charset_set_get(self):
        charset = ["a"]

        gen = TLGGenerator(charset)

        assert gen.charset == charset

        charset_2 = ["a", "b", "c"]

        gen.charset = charset_2

        assert gen.charset == charset_2

    @pytest.mark.parametrize(
        "fst_name,attr", [("L.fst", "_L_FST"), ("T.fst", "_T_FST")]
    )
    def test_FST(self, fst_name, attr):

        sample_fst = fst.StdVectorFst().read(f"./test/test_fsts/{fst_name}")
        gen = TLGGenerator(["a", "b", "c"])

        assert fst.isomorphic(
            sample_fst, getattr(gen, attr)
        ), f"FST aren't isomophic for {fst_name}"

    def test_generator(self, grammar):

        sample_fst = fst.StdVectorFst().read("./test/test_fsts/TLG.fst")
        gen = TLGGenerator(["a", "b"])

        grammar.set_input_symbols(gen.labels)
        grammar.set_output_symbols(gen.labels)

        output_fst = gen.generate_TLG(grammar)

        assert fst.isomorphic(sample_fst, output_fst)

    @pytest.mark.xfail(raises=TLGException)
    def test_bad_charset(self, grammar):
        gen = TLGGenerator(["c"])

        grammar.set_input_symbols(gen.labels)
        grammar.set_output_symbols(gen.labels)

        gen.generate_TLG(grammar)
