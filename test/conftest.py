import kaldi.fstext as fst
import pytest


@pytest.fixture
def grammar():
    grammar = fst.StdVectorFst()
    state0 = grammar.add_state()
    state1 = grammar.add_state()
    state2 = grammar.add_state()
    grammar.set_start(state0)
    grammar.set_final(state2)
    weight_free = fst.TropicalWeight(0.0)
    grammar.add_arc(state0, fst.StdArc(1, 1, weight_free, state1))
    grammar.add_arc(state1, fst.StdArc(2, 2, weight_free, state2))
    return grammar
