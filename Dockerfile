FROM conda/miniconda3:latest

RUN apt update && apt install make && \
	conda update -y -n base -c defaults conda && \
	conda install -yc pykaldi pykaldi-cpu && \
	conda install -yc conda-forge thrax pynini && \
	conda install -y pytest sphinx && \
	pip install sphinx-rtd-theme 


