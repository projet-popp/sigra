from typing import List

from kaldi import fstext as fst
from kaldi.fstext import special as fst_special
from kaldi.fstext import utils as fst_utils


class TLGGenerator:
    """
    Main class used for TLG generation for the kaldi decoder with CTC lattices.
    This creates a decoding graph for a specified charset.
    """

    def __init__(self, charset: List[str]):
        self._charset = charset
        self._create_symtables()
        self._create_L_FST()
        self._create_T_FST()

    @property
    def charset(self):
        """
        :type: List[str]

        Charset attribute.
        When changed updates symbol tables and L, T FSTs
        """
        return self._charset

    @charset.setter
    def charset(self, charset: List[str]):
        """
        Updates charset and re generates Lexicon and Tokens FSTs and symbol tables
        """
        self._charset = charset
        self._create_symtables()
        self._create_L_FST()
        self._create_T_FST()

    @property
    def labels(self):
        """
        :type: kaldi.fstext.SymbolTable

        Labels symbol table.
        It is the symbol table used by the Lexicon and Grammar FST.
        For the Lexicon FST, it is the **output** table.
        """
        return self._labels

    @property
    def tokens(self):
        """
        :type: kaldi.fstext.SymbolTable

        Tokens symbol table.
        It is the symbol table used by the Tokens and Lexicon FSTs.
        For the Lexicon FST, it is the **input** table.
        """
        return self._tokens

    def generate_TLG(self, grammar: fst.StdVectorFst) -> fst.StdVectorFst:
        """
        :param kaldi.fstext.StdVectorFst grammar: Grammar FST, corresponds to the G in TLG.
        :return: Returns the TLG FST
        :rtype: kaldi.fstext.StdVectorFst
        :raises TLGException: If the grammar symbol tables aren't the same as the :attr:`labels` symbol table

        Generates the TLG FST. The input grammar must have the same symbol table as the :attr:`labels` symbol table.
        """
        # Checking symTables before proceeding
        if not (
            fst.compat_symbols(self.labels, grammar.input_symbols())
            and fst.compat_symbols(self.labels, grammar.output_symbols())
        ):
            raise TLGException("Bad SymTables for grammar!")

        LG = fst.StdVectorFst()
        fst_special.table_compose(self._L_FST, grammar, LG)
        LG = fst.determinize(LG)
        LG.rmepsilon()
        fst_utils.minimize_encoded_std_fst(LG)
        LG.arcsort("ilabel")

        TLG = fst.StdVectorFst()
        fst_special.table_compose(self._T_FST, LG, TLG)

        return TLG

    def _create_symtables(self):
        tokens = fst.SymbolTable()
        tokens.set_name("Tokens")

        labels = fst.SymbolTable()
        labels.set_name("Labels")

        tokens.add_symbol("<epsilon>")
        labels.add_symbol("<epsilon>")
        tokens.add_symbol("<blk>")

        for char in self.charset:
            tokens.add_symbol(char)
            labels.add_symbol(char)

        self._tokens = tokens
        self._labels = labels

    def _create_L_FST(self):
        """
        Create Lexicon FST for translating Tokens to Labels.

        It maps each char in Tokens to corresponding char in Labels except <eps> and <blk>

        See https://gitlab.com/projet-popp/optikaldi/-/issues/5/ for more info about this FST
        """
        L_FST = fst.StdVectorFst()

        L_FST.set_input_symbols(self._tokens)
        L_FST.set_output_symbols(self._labels)

        state = L_FST.add_state()
        L_FST.set_start(state)
        L_FST.set_final(state)

        num_sym = self._tokens.num_symbols()

        for i in range(1, num_sym - 1):
            L_FST.add_arc(0, fst.StdArc(i + 1, i, fst.TropicalWeight(0.0), 0))

        L_FST.arcsort("olabel")

        self._L_FST = L_FST

    def _create_T_FST(self):
        """
        Creates the Tokens FST.
        """
        T_FST = fst.StdVectorFst()

        T_FST.set_input_symbols(self._tokens)
        T_FST.set_output_symbols(self._tokens)

        # creating fixed states
        state0 = T_FST.add_state()
        T_FST.set_start(state0)
        T_FST.set_final(state0)

        state1 = T_FST.add_state()
        state2 = T_FST.add_state()

        # Creating fixed arcs
        weight_free = fst.TropicalWeight(0.0)
        # <eps>:<eps> arcs
        T_FST.add_arc(state0, fst.StdArc(0, 0, weight_free, state1))
        T_FST.add_arc(state2, fst.StdArc(0, 0, weight_free, state0))
        # <blk>:<eps> arcs
        T_FST.add_arc(state1, fst.StdArc(1, 0, weight_free, state1))
        T_FST.add_arc(state2, fst.StdArc(1, 0, weight_free, state2))

        # start iteration over last symbols
        for i in range(2, self._tokens.num_symbols()):
            char_state = T_FST.add_state()
            T_FST.add_arc(state1, fst.StdArc(i, i, weight_free, char_state))
            T_FST.add_arc(char_state, fst.StdArc(i, 0, weight_free, char_state))
            T_FST.add_arc(char_state, fst.StdArc(0, 0, weight_free, state2))

        T_FST.arcsort("olabel")

        self._T_FST = T_FST


class TLGException(Exception):
    """
    Simple Exception Class for the TLG module
    """

    def __init__(self, message):
        self.message = message
