#!/usr/local/bin/python

import argparse
import pathlib
import pickle

import pynini


def fst_accept_test(string_to_test: str, input_fst: pynini.Fst):
    with pynini.default_token_type("utf8"):
        temp = string_to_test.strip()
        acceptor = pynini.accep(temp, token_type="utf8")
        accepted = pynini.compose(acceptor, input_fst, "alt_sequence")
    return accepted.start() != pynini.NO_STATE_ID


def parse_input_file(input_file: str):
    with open(input_file, "r") as f:
        for line in f:
            if fst_accept_test(line, input_fst):
                output["accepted_count"] += 1
            else:
                output["unaccepted_count"] += 1
                output["refused_list"].append(line.strip())

    output["total"] = output["accepted_count"] + output["unaccepted_count"]
    output["coverage"] = output["accepted_count"] / output["total"]
    print(f"Accepted {output['accepted_count']}, Refused {output['unaccepted_count']}")
    print(f"Coverage {output['coverage']}")
    return output


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("fst_file", type=pathlib.Path, help="Fst to test")

    parser.add_argument(
        "-i", "--interactive", action="store_true", help="Launch in interactive mode"
    )

    parser.add_argument(
        "-f",
        "--input_file",
        type=str,
        help="Text file containing words to pass through the fst",
    )

    parser.add_argument(
        "-o", "--output", type=pathlib.Path, help="Save the results in pickle file"
    )

    args = parser.parse_args()

    input_fst = pynini.Fst.read(str(args.fst_file))

    output = {}
    output["accepted_count"] = 0
    output["unaccepted_count"] = 0
    output["refused_list"] = []

    if args.interactive:
        print("Type ctrl+D to exit")
        try:
            s = input("--> ")
            while True:
                with pynini.default_token_type("utf8"):
                    print(fst_accept_test(s, input_fst))
                s = input("--> ")
        except EOFError:
            print("\n Exiting")
            exit(0)

    if args.input_file:
        output = parse_input_file(args.input_file)

    if args.output:
        with open(args.output, "wb+") as f:
            pickle.dump(output, f)
