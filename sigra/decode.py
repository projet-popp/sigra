import logging

from kaldi import decoder as kdecoder
from kaldi import fstext as fst
from kaldi import matrix as kmatrix
from kaldi.fstext import utils as fst_utils


class Decoder:
    """
    Class for decoding lattices
    """

    def __init__(
        self,
        TLG: fst.StdVectorFst,
        allow_partial: bool = True,
        accoustic_scale: float = 0.1,
        beam: float = 16.0,
        min_active: int = 20,
        max_active: int = 2 ** 31 - 1,
    ):
        self._TLG = TLG
        self._allow_partial = allow_partial
        self._accoustic_scale = accoustic_scale
        self._opts = kdecoder.FasterDecoderOptions()
        self._opts.beam = beam
        self._opts.min_active = min_active
        self._opts.max_active = max_active
        self._decoder = kdecoder.FasterDecoder(TLG, self._opts)

    @property
    def TLG(self):
        """
        :type: kaldi.fstext.StdVectorFst

        Decoding TLG graph.
        Regenerates the decoder when updated
        """
        return self._TLG

    @TLG.setter
    def TLG(self, TLG: fst.StdVectorFst):
        self._TLG = TLG
        self._decoder = kdecoder.FasterDecoder(TLG, self._opts)

    @property
    def allow_partial(self):
        """
        :type: bool

        True if  partial decoding is allowed.
        When partial decoding is allowed a warning will be issued if the decoding is not complete, and the return string won't be None.
        """
        return self._allow_partial

    @allow_partial.setter
    def allow_partial(self, allow_partial: bool):
        self._allow_partial = allow_partial

    @property
    def accoustic_scale(self):
        """
        :type: float

        Sets the accoustic scale for the decodable matrix.
        """
        return self._accoustic_scale

    @accoustic_scale.setter
    def accoustic_scale(self, accoustic_scale: float):
        self._accoustic_scale = accoustic_scale

    @property
    def beam(self):
        """
        :type: float

        Sets the beam search width.
        Larger is slower but more accurate.
        """
        return self._opts.beam

    @beam.setter
    def beam(self, beam: float):
        self._opts.beam = beam
        self._decoder.set_options(self._opts)

    @property
    def min_active(self):
        """
        :type: int

        Min number of active states. No pruning if active < min_active.
        """
        return self._opts.min_active

    @min_active.setter
    def min_active(self, min_active: int):
        self._opts.min_active = min_active
        self._decoder.set_options(self._opts)

    @property
    def max_active(self):
        """
        :type: int

        Max number of active states. Larger -> slower but more accurate.
        """
        return self._opts.max_active

    @max_active.setter
    def max_active(self, max_active: int):
        self._opts.max_active = max_active
        self._decoder.set_options(self._opts)

    def decode_one_matrix(self, loglike_matrix):
        """
        :param obj(matrix_like): A matrix, a 2-D numpy array, any object exposing a 2-D array interface, an object with an __array__ method returning a 2-D numpy array, or any (nested) sequence that can be interpreted as a matrix.
        :return: Returns the decoded string or None
        :rtype: str or None

        This functions decodes one loglike matrix and returns the decoded string, or None if partial decoding is disabled.
        """
        matrix = kmatrix.Matrix(loglike_matrix)
        decodable = kdecoder.DecodableMatrixScaled(matrix, self.accoustic_scale)
        self._decoder.decode(decodable)

        if self.allow_partial or self._decoder.reached_final():

            if not self._decoder.reached_final():
                logging.debug(
                    "Decoder did not reach end-state, outputting partial traceback."
                )

            decoded = self._decoder.get_best_path()

            (isym, osym, weights) = fst_utils.get_linear_symbol_sequence(decoded)

            out = fst.indices_to_symbols(self._TLG.output_symbols(), osym)
            return "".join(out)
        else:
            logging.error("Decoding failed, returning None")
            return None
