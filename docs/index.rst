.. SIGRA documentation master file, created by
   sphinx-quickstart on Thu Aug 26 14:00:37 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SIGRA's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
			 
   autodoc/index
   examples/example.rst
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
