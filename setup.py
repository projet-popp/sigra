import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="sigra",
    version="0.1.0",
    author="Nicolas Kempf",
    author_email="nicolas.kempf@hotmail.fr",
    description="A pykaldi and pynini/thrax wrapper for CTC Lattices",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/projet-popp/sigra",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3"
    ],
    python_requires=">=3.7",
)
