# SIGRA, a pykaldi wrapper and tools for CTC lattices

SIGRA means **SI**mple CTC **GRA**mmar toolkit

SIGRA helps you to create the right automata for CTC lattices and then decode the lattices.

# Documentation

Click [here](https://projet-popp.gitlab.io/sigra/) for a redacted and autodoc documentation.
There is also docstring and examples in [./examples/](./examples/)

# Installation

**Python 3.7 is the only version tested that ensures maximum interoperability between packages**

## Dependencies
The recommended installation is via a docker/podman using the given [dockefile](Dockerfile).

You can also use conda/miniconda to fetch the dependencies.

Manual installation is also available if you read the install guide of the dependencies.

Dependencies:
- [pykaldi](https://github.com/pykaldi/pykaldi)
- [thrax](https://www.openfst.org/twiki/bin/view/GRM/Thrax) (optional) for grammar creation
- [pynini](https://www.openfst.org/twiki/bin/view/GRM/Pynini) (optional) for grammar creation
- [pytest](https://docs.pytest.org/en/6.2.x/) (optional) for running tests
- [sphinx](https://www.sphinx-doc.org/en/master/) and sphinx-rtd-theme (optional) for building docs

## Install the module

Finally install the module with `pip install .`


